

# copylight
- `tools/kz_xmodem` is copy of `https://osdn.net/projects/kz-xmodem/` lisence detail for `src/kz_xmodem.c` in untar'ed files.
- `tools/h8write` is copy `http://mes.osdn.jp/h8/writer-j.html` under `GPL2`
- almost of other files are copy of Kozos `http://kozos.jp/kozos/` 
- thank you for all predecessors.


# メモ
### h8

DIPスイッチ4Pと動作のモードについて､ONに対して1234が､
- 上下上下 ならば シリアル通信モード
- 上上下上 ならb フラッシュ書き込みモード

シリアル通信ツールは､ ```sudo minicom -o```  

```sudo minicom -o -s``` より､`/etc/`に､`minirc.df1`が設定された｡




# 参考文献

1. 12ステップで作る 組み込みSO自作入門
1. 30日で作る OS自作入門
1. Structure and Interpretation of computer programs
1. FPGAボードで学ぶ組込みシステム開発入門 (Intel編)